<!--=============================================
=            Include JavaScript files           =
==============================================-->

<!-- Bootstrap V5 -- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>

<!-- Custom Js -->
<script type="text/javascript" src="<?php echo SERVERURL; ?>vistas/assets/js/main.js"></script>
