<!--=============================================
=            Include Css files           =
==============================================-->
<!-- Var -->
<link rel="stylesheet" href="./vistas/assets/css/var.css">

<!-- Bootstrap: Base -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<!-- Icons: FontAwesome -->
<script src="https://kit.fontawesome.com/d424c87d70.js" crossorigin="anonymous"></script>

<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/styles.css">


